﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.IO.Pipes;

public class EncryptedInputSender : MonoBehaviour
{
    //private string sharedKey = "MySharedKey123"; // Shared secret key for encryption and decryption // Doesn't work
    byte[] _key = { 0x00, 0x11, 0x22, 0x33, 044, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

    LockstepManager _lockstepManager;

    private void Start()
    {
        _lockstepManager = FindObjectOfType<LockstepManager>();
        if (!_lockstepManager)
            Debug.LogError("EncryptedInputSender -> No LockstepManager object found in scene");
    }

    private void Update()
    {
        // Simulate input
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Get input data
            string inputData = "Space";

            // Encrypt the input data
            byte[] encryptedData = EncryptData(inputData);

            // Send the encrypted data over the network (Replace with your network transmission code)
            SendDataOverNetwork(encryptedData);
        }
    }

    public void SendPlayerInfo(PlayerInfo playerInfo)
    {
        string json = JsonUtility.ToJson(playerInfo);
        Debug.Log("JSON formatting: " + json);
        byte[] encryptedData = EncryptData(json);
        SendDataOverNetwork(encryptedData);
    }

    //THROWING ERROR
    //private byte[] EncryptData(string data)
    //{
    //    byte[] encryptedData;

    //    using (Aes aes = Aes.Create())
    //    {
    //        // Derive a key and initialization vector (IV) from the shared key
    //        byte[] keyBytes = Encoding.UTF8.GetBytes(sharedKey);
    //        byte[] iv = new byte[aes.BlockSize / 8];
    //        Array.Copy(keyBytes, iv, iv.Length);

    //        // Set AES settings
    //        aes.Key = keyBytes;
    //        aes.IV = iv;
    //        aes.Mode = CipherMode.CBC;

    //        // Create an encryptor to perform the AES encryption
    //        ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

    //        // Convert the input string to bytes
    //        byte[] dataBytes = Encoding.UTF8.GetBytes(data);

    //        // Encrypt the data
    //        using (MemoryStream memoryStream = new MemoryStream())
    //        {
    //            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
    //            {
    //                cryptoStream.Write(dataBytes, 0, dataBytes.Length);
    //                cryptoStream.FlushFinalBlock();
    //            }

    //            encryptedData = memoryStream.ToArray();
    //        }
    //    }

    //    return encryptedData;
    //}
    private byte[] EncryptData(string data)
    {
        byte[] encryptedData;
        byte[] dataBytes = Encoding.UTF8.GetBytes(data);
        using (Aes aes = Aes.Create())
        {
            aes.Key = _key;

            byte[] iv = aes.IV;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                memoryStream.Write(iv, 0, iv.Length);
                using (CryptoStream cryptoStream = new CryptoStream(
                    memoryStream,
                    aes.CreateEncryptor(),
                    CryptoStreamMode.Write))
                {
                    cryptoStream.Write(dataBytes, 0, dataBytes.Length);
                    cryptoStream.FlushFinalBlock();
                }
                encryptedData = memoryStream.ToArray(); 
            }
        }
        return encryptedData;
    }

    private void SendDataOverNetwork(byte[] data)
    {
        // Replace this method with your network transmission code to send the encrypted data
        // over the network to the intended recipient(s)
        Debug.Log("Encrypted Data: " + BitConverter.ToString(data));
        if (_lockstepManager)
        {
            _lockstepManager.AddToBuffer(data);
            Debug.Log("Sending encrypted data to lockstep...");
        }
    }
}