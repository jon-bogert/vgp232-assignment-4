using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayManager : MonoBehaviour
{

    [SerializeField] TMP_InputField _firstName;
    [SerializeField] TMP_InputField _lastName;
    [SerializeField] TMP_Dropdown _gender;
    [SerializeField] Slider _age;
    [SerializeField] TMP_Text _ageLabel;

    [SerializeField] PlayerInfo _info;

    public PlayerInfo Info { get { return _info; } }

    void Start()
    {
        UpdateDisplay();
    }

    void UpdateDisplay()
    {
        _firstName.text = _info.firstName;
        _lastName.text = _info.lastName;
        _gender.value = (int)_info.gender;
        _age.value = _info.age;
        _ageLabel.text = "Age: " + _info.age;
    }

    public void SendData()
    {
        FindObjectOfType<EncryptedInputSender>().SendPlayerInfo(_info);
    }

    public void UpdateAge()
    {
        _info.age = (byte)_age.value;
        UpdateDisplay();
    }

    public void UpdateGender()
    {
        _info.gender = (Gender)_gender.value;
        UpdateDisplay();
    }

    public void UpdateFirstName()
    {
        _info.firstName = _firstName.text;
        UpdateDisplay();
    }
    public void UpdateLastName()
    {
        _info.lastName = _lastName.text;
        UpdateDisplay();
    }




}
