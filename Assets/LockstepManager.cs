﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

public class LockstepManager : MonoBehaviour
{
    private float fixedDeltaTime = 0.02f; // Lockstep fixed time step

    private float lockstepTimer = 0f; // Timer to track lockstep frame progression
    private int lockstepFrame = 0; // Current lockstep frame count

    private List<InputData> inputBuffer = new List<InputData>(); // Buffer to store inputs received from clients

    private void Start()
    {
        // Initialize networking here
    }

    private void Update()
    {
        // Check for new input and add it to the buffer
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InputData inputData = new InputData();
            inputData.frame = lockstepFrame;
            inputData.input = Encoding.UTF8.GetBytes("Space");
            inputBuffer.Add(inputData);
        }

        // Process the lockstep frame if the timer exceeds the fixed time step
        lockstepTimer += Time.deltaTime;
        if (lockstepTimer >= fixedDeltaTime)
        {
            // Process the lockstep frame
            ProcessLockstepFrame();

            // Increment lockstep frame and reset the timer
            lockstepFrame++;
            lockstepTimer = 0f;
        }
    }

    public void AddToBuffer(byte[] data)
    {
        InputData inputData = new InputData();
        inputData.input = data;
        inputData.frame = lockstepFrame;
        inputBuffer.Add(inputData);
    }

    private void ProcessLockstepFrame()
    {
        // Retrieve and process inputs for the current frame from the buffer
        List<InputData> inputsToProcess = inputBuffer.FindAll(input => input.frame == lockstepFrame);
        foreach (InputData inputData in inputsToProcess)
        {
            // Process input
            Debug.Log($"LockstepManager -> Processing input '{BitConverter.ToString(inputData.input)}' for frame {inputData.frame}");
        }

        // Simulate game logic for the lockstep frame
        SimulateLockstepFrame();
    }

    private void SimulateLockstepFrame()
    {
        // Simulate game logic for the lockstep frame
        //Debug.Log($"Simulating lockstep frame {lockstepFrame}");    //// *** BOGGING DOWN THE CONSOLE ***

        // Send synchronized data and inputs to clients
        SendSynchronizedData();
    }

    private void SendSynchronizedData()
    {
        // Send synchronized game state data and inputs to clients
        // You can use your networking implementation to send the data
        // The data can include information about game objects, positions, states, and processed inputs
        // Ensure the data is sent to clients at the appropriate time for lockstep synchronization
    }
}

// Example struct for input data
public struct InputData
{
    public int frame;
    public byte[] input;
}
