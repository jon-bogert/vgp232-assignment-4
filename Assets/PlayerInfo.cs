using System;

public enum Gender { Male, Female, Other }

[Serializable]
public class PlayerInfo
{
    public string firstName = "";
    public string lastName = "";
    public Gender gender = Gender.Other;
    public byte age = 0;
}
